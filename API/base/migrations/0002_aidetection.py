# Generated by Django 4.2.5 on 2023-10-17 13:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AIDetection',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('detection_results', models.FloatField()),
                ('uploaded_file', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='base.filemodel')),
            ],
        ),
    ]
